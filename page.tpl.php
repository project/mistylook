<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <?php if (theme_get_setting('iepngfix_setting')): ?>
    <!--[if lte IE 6]>
      <script type="text/javascript">
        jQuery(function($) {
          $("img[@src$=png]").pngfix();
        });
      </script>
    <![endif]-->
  <?php endif; ?>
</head>

<body>
  <?php if ($primary_links || $search_box): ?>
    <div id="navigation">
      <?php if ($primary_links): ?>
        <?php print menu_tree(variable_get('menu_primary_links_source', 'primary-links')); ?>
      <?php endif; ?>
      <!-- /primary_menu -->
      <?php if ($search_box): ?>
        <div class="search">
          <?php print $search_box; ?>
        </div>
      <?php endif; ?>
      <!-- /search-box -->
    </div>
    <!-- /navigation -->
  <?php endif; ?>

  <div id="container">
    <?php if ($site_name || $site_slogan): ?>
      <div id="header">
        <?php if ($site_name): ?>
          <h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <p class='site-slogan'><?php print $site_slogan ?></p>
        <?php endif; ?>
      </div>
      <!-- /header -->
    <?php endif; ?>

    <div id="headerimage">
    </div>
    <!-- /headerimage -->
    <div id="main">
      <div id="main-wrapper">
        <?php if ($breadcrumb):?>
          <?php print $breadcrumb ?>
        <?php endif; ?>

        <?php if ($messages): ?>
          <?php print $messages; ?>
        <?php endif; ?>

        <?php if ($title) : ?>
         <h2 class="title"><?php print $title ?></h2>
        <?php endif ?>

        <?php if ($tabs): ?>
        <div id="content-tabs">
          <?php print $tabs; ?>
        </div>
        <?php endif; ?>

        <?php if ($help): ?>
          <div id="help">
            <?php print $help; ?>
          </div>
        <?php endif; ?>

        <?php if ($content_top): ?>
          <div id="content-top">
            <?php print $content_top ?>
          </div>
          <!-- /content-top -->
        <?php endif; ?>

        <?php print $content ?>
      </div>
      <!-- /main-wrapper -->
      <?php if ($right): ?>
        <div id="sidebar">
          <?php print $right; ?>
        </div>
        <!-- /sidebar -->
      <?php endif; ?>

    </div>
    <!-- /main -->
  </div>
  <!-- /container -->

  <?php if ($footer || $footer_message): ?>
    <div id="footer">
      <?php if ($footer_message): ?>
        <?php print $footer_message; ?>
      <?php endif; ?>

      <?php if ($footer): ?>
        <?php print $footer; ?>
      <?php endif; ?>
    </div>
    <!-- /footer -->
  <?php endif; ?>

  <?php print $closure ?>
</body>
</html>
