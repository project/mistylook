<?php
?>

<?php if (isset($comments_count)): ?>
  <h3 id="comment-response"><?php print $comments_count ?></h3>
<?php endif; ?>
<div id="comments">
  <?php print $content; ?>
</div>